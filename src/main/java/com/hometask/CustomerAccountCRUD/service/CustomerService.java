package com.hometask.CustomerAccountCRUD.service;

import com.hometask.CustomerAccountCRUD.model.Customer;
import com.hometask.CustomerAccountCRUD.persistence.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public String hello() {
        return "Welcome to Customer Account, enter your request";
    }


    public Customer findById(String id) {
        return customerRepository.findById(id).orElseThrow(() -> new RuntimeException("Customer not found !!"));
    }

    public Iterable<Customer> findAll() {
        return  customerRepository.findAll();
    }

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer update(Customer customer) {
        return customerRepository.save(customer);
    }

    public Iterable<Customer> delete(String id) {
        customerRepository.delete(findById(id));
        return findAll();
    }

    public void deleteRepo() {
        customerRepository.deleteAll();
    }
}