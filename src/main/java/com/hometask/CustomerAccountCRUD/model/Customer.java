package com.hometask.CustomerAccountCRUD.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@RedisHash("Customer")
public class Customer {

    private String id;
    private String name;
    private long shoppingSum;

}
