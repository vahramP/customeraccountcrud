package com.hometask.CustomerAccountCRUD;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CustomerAccountCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerAccountCrudApplication.class, args);
	}

}
