package com.hometask.CustomerAccountCRUD.controller;

import com.hometask.CustomerAccountCRUD.model.Customer;
import com.hometask.CustomerAccountCRUD.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("customer")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping("/hello")
    public String hello() {
        return customerService.hello();
    }

    @GetMapping("/{id}")
    public Customer findBiId(@PathVariable("id") String id) {
        return customerService.findById(id);
    }

    @GetMapping
    public Iterable<Customer> findAll() {
        return customerService.findAll();
    }

    @PostMapping
    public Customer save(@RequestBody Customer customer) {

        return customerService.save(customer);
    }

    @PutMapping("/{id}")
    public Customer update(@PathVariable String id, @RequestBody Customer customer) {
        customer.setId(id);

        return customerService.update(customer);
    }

    @DeleteMapping("/{id}")
    public Iterable<Customer> delete(@PathVariable("id") String id) {
        return customerService.delete(id);
    }

    @DeleteMapping("flushall")
    public void deleteRepo() {
        customerService.deleteRepo();
    }
}
