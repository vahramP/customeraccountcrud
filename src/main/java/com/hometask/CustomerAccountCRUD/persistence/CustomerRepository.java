package com.hometask.CustomerAccountCRUD.persistence;

import com.hometask.CustomerAccountCRUD.model.Customer;
import org.springframework.data.repository.CrudRepository;


public interface CustomerRepository extends CrudRepository<Customer, String> {

}
