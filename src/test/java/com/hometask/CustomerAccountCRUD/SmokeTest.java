package com.hometask.CustomerAccountCRUD;

import com.hometask.CustomerAccountCRUD.controller.CustomerController;
import com.hometask.CustomerAccountCRUD.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class SmokeTest {

    @Autowired
    private CustomerController controller;

    @Autowired
    private CustomerService service;

    @Test
    public void contextLoads() throws Exception {
        assertNotNull(controller);
        assertNotNull(service);
    }
}
