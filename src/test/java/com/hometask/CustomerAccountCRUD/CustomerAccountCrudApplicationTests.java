package com.hometask.CustomerAccountCRUD;

import com.hometask.CustomerAccountCRUD.model.Customer;
import com.hometask.CustomerAccountCRUD.service.CustomerService;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.Jedis;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CustomerAccountCrudApplicationTests {

	Jedis jedisCon = new Jedis();

	@Autowired
	CustomerService customerService;

	@Test
	public void contextLoads() {
		AssertionsForClassTypes.assertThat(customerService).isNotNull();
	}

	@Test
	public void createDbConnection() {
		// When connecting to database
		jedisCon.ping();

		// Then close the connection
		jedisCon.close();
	}

	@Test
	public void save() {
		// Given new customer data
		customerService.save(new Customer("1000000", "Vladimir", 200000L));

		// When searching customer by given parameters
		String testCustomerName = customerService.findById("1000000").getName();
		long testCustomerShoppingSum = customerService.findById("1000000").getShoppingSum();

		// Then assert customer saved
		assertEquals("Vladimir", testCustomerName);
		assertEquals(200000L, testCustomerShoppingSum);

		customerService.delete("1000000");
	}

	@Test
	public void findByID() {
		// Given new customer data
		customerService.save(new Customer("1000001", "Vahram", 2000002L));

		// When retrieving customer data
		String name = customerService.findById("1000001").getName();
		long shoppingSum = customerService.findById("1000001").getShoppingSum();

		// Then assert customer data exists
		assertEquals("Vahram", name);
		assertEquals(2000002L, shoppingSum);

		customerService.delete("1000001");
	}

	@Test
	public void findAll() {
		// Given link to customer repo
		String url = "http://localhost:8085";

		// When retrieving all data
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response	= restTemplate.getForEntity(url + "/customer", String.class);

		// Then assert response status is OK
		assertEquals(response.getStatusCode(), HttpStatus.OK);
	}

	@Test
	public void update() {
		// Given new customer data
		customerService.save(new Customer("1000002", "Vahram", 15000051L));

		// When updating customer data
		customerService.update(new Customer("1000002", "VAHRAM", 15001051L));
		String updatedName = customerService.findById("1000002").getName();
		Long updatedShoppingSum = customerService.findById("1000002").getShoppingSum();

		// Then assert data updated
		assertEquals("VAHRAM", updatedName);
		assertEquals(15001051L, updatedShoppingSum);

		customerService.delete("1000002");

	}

	@Test()
	public void delete() {
		// Given new customer data
		customerService.save(new Customer("1000003", "Vahram", 200000L));

		// When deleting customer
		customerService.delete("1000003");

		// Then assert customer doesn't exist
		Assertions.assertThrows(RuntimeException.class, () -> customerService.findById("1000003"));
	}

	@Test
	public void flushAll() {
		// When deleting all customer data
		jedisCon.flushAll();

		// Then assert all data deleted
		assertEquals(0, (long)jedisCon.dbSize());
	}

}
