package com.hometask.CustomerAccountCRUD;

import com.hometask.CustomerAccountCRUD.controller.CustomerController;
import com.hometask.CustomerAccountCRUD.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomerController.class)
public class WebLayerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerService service;

    @Test
    public void shouldReturnAllCustomers() throws Exception {
        mockMvc.perform(get("/customer/all")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void shouldReturnMessageFromService() throws Exception {
        when(service.hello()).thenReturn("Welcome to Customer Account, enter your request");

        this.mockMvc
                .perform(get("/customer/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Welcome to Customer Account, enter your request")));
    }
}
